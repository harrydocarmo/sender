package com.devmdocarmo.sender

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                // Aqui se recibiria la informacion procesada
                val data: Intent? = result.data
                simple_text.text = data?.getStringExtra(Intent.EXTRA_TEXT).toString()

            }
        }
        val json = getJSON()

        button_send.setOnClickListener {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, json)
                type = "text/plain"
            }

            // Try to invoke the intent.
            try {

                resultLauncher.launch(sendIntent)

            } catch (e: ActivityNotFoundException) {
                // Define what your app should do if no activity can handle the intent.
                Toast.makeText(this, "No se pudo compartir el archivo", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun getJSON(): String {
        return  "\n" +
                "{\n" +
                "\t\"tipoDocElectronico\": \"1\",\n" +
                "\t\"timbrado\": \"12558934\",\n" +
                "\t\"fechaIniTimb\": \"25/08/2021 00:00:00\",\n" +
                "\t\"numeroComprobante\": \"001-001-9000032\",\n" +
                "\t\"fechaHora\": \"10/10/2022 17:49:43\",\n" +
                "\t\"tipoTransaccion\": \"2\",\n" +
                "\t\"tipoImpuestoAfectado\" : \"1\",\n" +
                "\t\"tipoOperacion\": \"1\",\n" +
                "\t\"moneda\": \"PYG\",\n" +
                "\t\"tipoCambio\": null,\n" +
                "\t\"tipoReceptor\": 1,\n" +
                "\t\"tipoContribuyenteReceptor\": 1,\n" +
                "\t\"rucReceptor\": \"4317866-9\",\n" +
                "\t\"nombreRazonSocial\": \"Saul Alfredo Soto Galeano\",\n" +
                "\t\"correoElectronico\": \"saul@kiga.com.py,jromero@tdn.com.py,amigleon92@gmail.com\",\n" +
                "\t\"telefono\": null,\n" +
                "\t\"direccion\": null,\n" +
                "\t\"condicionVenta\": 2,\n" +
                "\t\"entregaInicial\": null,\n" +
                "\t\"cuotas\": 1,\n" +
                "\t\"plazos\": null,\n" +
                "\t\"gravada10\": 25000,\n" +
                "\t\"iva10\": 2500,\n" +
                "\t\"gravada5\": 10000,\n" +
                "\t\"iva5\": 500,\n" +
                "\t\"exento\": 0,\n" +
                "\t\"totalDescuento\": 0,\n" +
                "\t\"total\": 38000,\n" +
                "\t\"motivoEmision\": null,\n" +
                "\t\"infoInteresEmisor\": null ,\n" +
                "\t\"infoInteresFiscal\": null,\n" +
                "\t\"codigoDepartamentoReceptor\": \"1\",\n" +
                "\t\"departamentoReceptor\": \"CAPITAL\",\n" +
                "\t\"codigoDistritoRecptor\": \"1\",\n" +
                "\t\"distrito\": \"ASUNCION (DISTRITO)\",\n" +
                "\t\"codigoCiudadReceptor\": \"1\",\n" +
                "\t\"descripcionCiudadReceptor\": \"ASUNCION (DISTRITO)\",\n" +
                "\t\"celularReceptor\": null,\n" +
                "\t\"codigoCliente\": null,\n" +
                "\t\"codigoPais\": \"PRY\",\n" +
                "\t\"numeroCasaReceptor\": null,\n" +
                "\t\"nombreFantasiaReceptor\": \"Nombre Fantasia Receptor\",\n" +
                "\t\"detalle\": [\n" +
                "\t\t{\n" +
                "\t\t\t\"codigo\": \"7840058000019\",\n" +
                "\t\t\t\"cantidad\": 5,\n" +
                "\t\t\t\"unidadMedida\": \"77\",\n" +
                "\t\t\t\"descripcion\": \"Producto de Prueba UND IVA 10%\",\n" +
                "\t\t\t\"precioUnitario\": 5500,\n" +
                "\t\t\t\"importeItems\": 27500,\n" +
                "\t\t\t\"porcentajeDescuento\": 0,\n" +
                "\t\t\t\"importeDescItems\": 0,\n" +
                "\t\t\t\"totalDescuentoItems\": 0,\n" +
                "\t\t\t\"tasaImpuesto\": \"10\",\n" +
                "\t\t\t\"gravado10\": 25000,\n" +
                "\t\t\t\"iva10\": 2500,\n" +
                "\t\t\t\"gravado5\": 0,\n" +
                "\t\t\t\"iva5\": 0,\n" +
                "\t\t\t\"exento\": 0,\n" +
                "\t\t\t\"total\": 27500\n" +
                "\t\t},\n" +
                "\t\t{\n" +
                "\t\t\t\"codigo\": \"1241211231231\",\n" +
                "\t\t\t\"cantidad\": 3,\n" +
                "\t\t\t\"unidadMedida\": 77,\n" +
                "\t\t\t\"descripcion\": \"Producto de Prueba UND EX\",\n" +
                "\t\t\t\"precioUnitario\": 3500,\n" +
                "\t\t\t\"importeItems\": 10500,\n" +
                "\t\t\t\"porcentajeDescuento\": 0,\n" +
                "\t\t\t\"importeDescItems\": 0,\n" +
                "\t\t\t\"totalDescuentoItems\": 0,\n" +
                "\t\t\t\"tasaImpuesto\": \"0\",\n" +
                "\t\t\t\"gravado10\": 0,\n" +
                "\t\t\t\"iva10\": 0,\n" +
                "\t\t\t\"gravado5\": 10000,\n" +
                "\t\t\t\"iva5\": 500,\n" +
                "\t\t\t\"exento\": 0,\n" +
                "\t\t\t\"total\": 10500\n" +
                "\t\t}\n" +
                "\t],\"pagos\" : null,\n" +
                "\t\"detalleCuotas\": [\n" +
                "\t\t{\n" +
                "\t\t\t\"cuota\": 1,\n" +
                "\t\t\t\"monedaCuota\": \"PYG\",\n" +
                "\t\t\t\"montoCuota\": 38000,\n" +
                "\t\t\t\"fechaVencimiento\": \"10/10/2020 00:00:00\",\n" +
                "\t\t\t\"idCabecera\": null\n" +
                "\t\t}\n" +
                "\t], \n" +
                "\t\t\t\"documentos\" : null\n" +
                "}\n"
    }
}